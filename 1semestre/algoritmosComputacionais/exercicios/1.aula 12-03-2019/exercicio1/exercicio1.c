#include <stdio.h>

main()
{

    float salarioBruto, qHoraTrabalhada, valHora;

    printf("Insira o valor da hora trabalhada: ");
    scanf("%f", &valHora);

    printf("Insira quantas horas foram trabalhadas: ");
    scanf("%f", &qHoraTrabalhada);

    salarioBruto = qHoraTrabalhada * valHora;

    printf("O salário bruto é R$: %.2f", salarioBruto);
    printf("\n");
}
