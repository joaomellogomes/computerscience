#include <stdio.h>

main(){

    float salBase = 0;

    printf("Insira o salário base de seu funcionário: \n");
    scanf("%f", &salBase);

    salBase = (salBase - (salBase * 0.1)) + 50;

    printf("Salário a receber \nR$%.2f", salBase);

}
