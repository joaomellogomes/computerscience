#include <stdio.h>

main(){

    int anoNasc = 0, anoAtual = 0;

    printf("\nInsira o ano atual: \n");
    scanf("%i", &anoAtual);

    printf("\nInsira seu ano de nascimento: \n");
    scanf("%i", &anoNasc);

    anoAtual = anoAtual - anoNasc;

    printf("\nVocê tem: %i", anoAtual);

    anoAtual = 2020 - anoNasc;

    printf(" anos.\n\nEm 2020 você terá: %i", anoAtual);
    printf(" anos.\n\n");

}
