#include <stdio.h>

main(){

	float custoFabrica = 0, perLucroDistribuidor = 0, perImpostos = 0;

	printf("\nInsira o custo de fábrica: \n");
	scanf("%f", &custoFabrica);

	printf("\n\nInsira o percentual de lucro do distribuidor (%): \n");
	scanf("%f", &perLucroDistribuidor);

	printf("\n\nInsira o percentual de impostos: \n");
	scanf("%f", &perImpostos);

	perLucroDistribuidor = (perLucroDistribuidor  / 100) * custoFabrica;

	printf("\n\nLucro do distribuidor:\nR$%.2f", perLucroDistribuidor);

	perImpostos = (perImpostos / 100) * custoFabrica;

	printf("\n\nValor dos impostos:\nR$%.2f", perImpostos);

	custoFabrica = custoFabrica + perImpostos + perLucroDistribuidor;

	printf("\n\nPreço final do veículo:\nR$%.2f", custoFabrica);

	printf("\n\n");

}
